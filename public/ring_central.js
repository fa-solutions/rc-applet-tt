let FAClient = null;
let RingCentral = null;
let notyf = null;
let currentMatchedContact = null;
let matchingContacts = false;
let ongoingCall = null;
let currentCallNumber = null;
let notificationOpen = false;
let leadApp = null;

const DIRECTION = {
  inbound: '6563c5c8-b57e-41d6-8ba6-be076b464219',
  outbound: 'd981eacd-2361-45c3-ad01-22bbb4871298'
};
// aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3JjLWFwcGxldC10dC8=
// aHR0cDovL2xvY2FsaG9zdDo1MDAw
const SERVICE = {
  name: 'FreeAgentService',
  appletId: `aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3JjLWFwcGxldC10dC8=`,
};

const PHONE_APPLET_CONFIGURATION = {
  name: 'ring_central',
  contact_app: 'lead',
  phone_field: 'lead_field4',
  name_field: 'lead_field11',
  last_name: 'lead_field12'
};

PHONE_APPLET_CONFIGURATION.fields = {
  type: `${PHONE_APPLET_CONFIGURATION.name}_field0`, // Phone
  direction: `${PHONE_APPLET_CONFIGURATION.name}_field1`, // Choice List Adhoc,
  from: `${PHONE_APPLET_CONFIGURATION.name}_field2`, // Phone
  to: `${PHONE_APPLET_CONFIGURATION.name}_field3`, // Phone
  lead: `${PHONE_APPLET_CONFIGURATION.name}_field4`, // Reference to Leads
  contact: `${PHONE_APPLET_CONFIGURATION.name}_field5`, // Reference to Contacts
  ringcentralId: `${PHONE_APPLET_CONFIGURATION.name}_field6`, // Text (unique)
  duration: `${PHONE_APPLET_CONFIGURATION.name}_field7`, // Number with mask `00:00:00`
  note: `${PHONE_APPLET_CONFIGURATION.name}_field8`, // Note
  smsSubject: `${PHONE_APPLET_CONFIGURATION.name}_field12`, // Note
  smsMessageStatus: `${PHONE_APPLET_CONFIGURATION.name}_field14`, // Choice List Adhoc,
  smsReadStatus: `${PHONE_APPLET_CONFIGURATION.name}_field15`, // Choice List Adhoc,
  smsMessageType: `${PHONE_APPLET_CONFIGURATION.name}_field16` // Choice List Adhoc,
};

const SMS_APPLET_CONFIGURATION = {
  name: 'ring_central',
};


const PHONE_FIELD_TYPES = [
  { field: PHONE_APPLET_CONFIGURATION.phone_field, type: 'business' },
];

function setCurrentMatchedContact(contact) {
  currentMatchedContact = contact;
}

function startupService() {
  notyf = new Notyf({
    duration: 20000,
    dismissible: true,
    position: {
      x: 'center',
      y: 'bottom',
    },
    types: [
      {
        type: 'info',
        className: 'info-notyf',
        icon: false,
      },
    ],
  });

  FAClient = new FAAppletClient({
    appletId: SERVICE.appletId,
  });

  FAClient.on('makeCall', (data) => {
    console.log(data);
    const phoneNumber = _.get(data, `record.field_values[${PHONE_APPLET_CONFIGURATION.phone_field}].value`);
    setCurrentMatchedContact(data.record);
    currentCallNumber = phoneNumber;
    FAClient.open();
    makeCall(phoneNumber);
  });

  FAClient.on('makeContactCall', (data) => {
    console.log(data);
    const phoneNumber = _.get(data, `record.field_values.work_phone.value`);
    setCurrentMatchedContact(data.record);
    currentCallNumber = phoneNumber;
    FAClient.open();
    makeCall(phoneNumber);
  });


  FAClient.on('sendSMS', (data) => {
    const phoneNumber = _.get(data, `record.field_values[${PHONE_APPLET_CONFIGURATION.phone_field}].value`);
    console.log(phoneNumber);
    FAClient.open();
    RingCentral.postMessage({
      type: 'rc-adapter-new-sms',
      phoneNumber,
    }, '*');
  });

  FAClient.on('sendContactSMS', (data) => {
    const phoneNumber = _.get(data, `record.field_values.work_phone.value`);
    console.log(phoneNumber);
    FAClient.open();
    RingCentral.postMessage({
      type: 'rc-adapter-new-sms',
      phoneNumber,
    }, '*');
  });

  FAClient.on('phone_field_clicked', ({ record, number }) => {
    setCurrentMatchedContact(record);
    currentCallNumber = number;
    makeCall(number);
  });

  const { clientId, clientSecret, redirectUri } = FAClient.params;
  console.log(clientId, clientSecret, redirectUri);
  if (clientId && clientSecret) {
    const iFrame = document.createElement('iframe');
    const appServer = 'https://platform.devtest.ringcentral.com';
    iFrame.src = `https://ringcentral.github.io/ringcentral-embeddable/app.html?appKey=${clientId}&appSecret=${clientSecret}&appServer=${appServer}&redirectUri=${redirectUri}`;
    iFrame.style = 'width: 100%; height: 100%; border: none;';
    iFrame.allow = 'microphone';
    window.addEventListener('message', ringCentralListener);
    removeTextMessage();
    document.getElementById('frameContainer').appendChild(iFrame);
    RingCentral = iFrame.contentWindow;
  }
}

function makeCall(phoneNumber) {
  RingCentral.postMessage(
    {
      type: 'rc-adapter-new-call',
      phoneNumber,
      toCall: true,
    },
    '*',
  );
}

function logMessagesManually(data) {
  const triggerType = _.get(data, 'body.triggerType');

  if (triggerType !== 'manual') {
    return RingCentral.postMessage(
      {
        type: 'rc-post-message-response',
        responseId: data.requestId,
        response: { data: 'ok' },
      },
      '*',
    );
  }

  const conversation = _.get(data, 'body.conversation');
  const messages = _.get(conversation, 'messages', []);
  const phoneNumbers = _.get(conversation, 'correspondents', [])
        .map((c) => c.phoneNumber);

  logMessages(messages, phoneNumbers, () => {
    RingCentral.postMessage(
      {
        type: 'rc-post-message-response',
        responseId: data.requestId,
        response: { data: 'ok' },
      },
      '*',
    );
  })
}

function logCallById(callId, callValues, callback) {
  FAClient.listEntityValues(
    {
      entity: PHONE_APPLET_CONFIGURATION.name,
      filters: [
        {
          field_name: PHONE_APPLET_CONFIGURATION.fields.ringcentralId,
          operator: 'includes',
          values: [callId],
        },
      ],
    },
    (existingPhoneCalls) => {
      const existingPhoneCall = _.get(existingPhoneCalls, '[0]');
      FAClient.upsertEntity({
        id: _.get(existingPhoneCall, 'id', ''),
        ...callValues,
      }, callback);
    },
  );
}

function logCallsManually(data) {
  const responseId = data.requestId;
  const triggerType = data.body && data.body.triggerType;

  if (triggerType) {
    return RingCentral.postMessage(
      {
        type: 'rc-post-message-response',
        responseId,
        response: { data: 'ok' },
      },
      '*',
    );
  }

  const callId = _.get(data, 'body.call.id');
  const callValues = getCallValuesFromData(data);

  logCallById(callId, callValues, () => {
    RingCentral.postMessage(
      {
        type: 'rc-post-message-response',
        responseId,
        response: { data: 'ok' },
      },
      '*',
    );
  });
}

function parseNumbersForPattern(numbers) {
  return numbers.reduce((prev, next) => {
    const parsedNumber = libphonenumber.parsePhoneNumberFromString(next);
    return [
      ...prev,
      ...(parsedNumber
        ? [parsedNumber.number, parsedNumber.nationalNumber]
        : []),
    ];
  }, [])
  .filter((n) => n && n.length > 4).join('|');
}

function parseNumbers(numbers) {
  let number = numbers.filter(n => n && n.length > 4);
  console.log({number})
  if (number && number.length > 0) {
     return number[0];
  } else {
    return null;
  }
}

function matchContacts(data) {
  console.log({matched: data})
  if (!ongoingCall) return;

  const phoneNumbers = data.body.phoneNumbers;

  if (currentMatchedContact) {
    return setMatchedContacts(currentMatchedContact, data, phoneNumbers);
  }

  if (phoneNumbers.length > 1) return;

  const pattern = parseNumbersForPattern(phoneNumbers);
  console.log(pattern);
  FAClient.listEntityValues(
    {
      entity: PHONE_APPLET_CONFIGURATION.contact_app,
      filters: [
          {
            field_name: PHONE_APPLET_CONFIGURATION.phone_field,
            operator: 'includes',
            values: [parseNumbers(phoneNumbers)],
          },
],
      limit: 1,
    },
    (contacts) => {
      console.log({matchContacts: contacts})
      //setCurrentMatchedContact(_.get(contacts, '[0]'));
      setMatchedContacts(currentMatchedContact, data, phoneNumbers);
    },
  );
}

function inboundContact(data) {
  console.log({inbound: data})
 // if (!ongoingCall) return;
  let phoneNumber = '';

  if (currentCallNumber) {
    phoneNumber = currentCallNumber;
  } else {

    if (data.call.fromNumber && data.call.fromNumber !== 'undefined') {
      phoneNumber = data.call.fromNumber;
    }
    if (!phoneNumber && data.call.from) {
      phoneNumber = data.call.from
    }
    if (phoneNumber.includes('*')) {
      phoneNumber = phoneNumber.split('*')[0];
    }
  }

  FAClient.listEntityValues(
      {
        entity: PHONE_APPLET_CONFIGURATION.contact_app,
        filters: [
          {
            field_name: PHONE_APPLET_CONFIGURATION.phone_field,
            operator: 'includes',
            values: [phoneNumber],
          },
        ],
        limit: 1,
      },
      (contacts) => {
        console.log({leadsFound: contacts})
        if(contacts && contacts.length > 0) {
          leadApp = 'lead';
          setCurrentMatchedContact(_.get(contacts, '[0]'));
          //setMatchedContacts(contact, data, phoneNumbers)
          renderContactButtton();
        } else {
          FAClient.listEntityValues(
              {
                entity: 'contact',
                filters: [
                  {
                    field_name: 'work_phone',
                    operator: 'includes',
                    values: [phoneNumber],
                  },
                ],
                limit: 1,
              },(contacts) => {
                console.log({contactsFound: contacts})
                if(contacts && contacts.length > 0) {
                  leadApp = 'contact';
                  setCurrentMatchedContact(_.get(contacts, '[0]'));
                  renderContactButtton();
                }
              });
        }
        //setMatchedContacts(currentMatchedContact, data, phoneNumbers);
      },
  );
}

function sendContactNotification(contact) {
  if (notificationOpen) return;
  notificationOpen = true;
  let notification = notyf.open({
    type: 'info',
    message: `Contact ${_.get(
      contact,
      `field_values[${PHONE_APPLET_CONFIGURATION.name_field}].value`,
    )} found! Click to navigate to record`,
  });

  notification.on('click', ({ target, event }) => {
    FAClient.navigateTo(`/${PHONE_APPLET_CONFIGURATION.contact_app}/view/${contact.id}`);
    notyf.dismiss(notification);
  });

  notification.on('dismiss', () => {
    notificationOpen = false;
  });
}

function setMatchedContacts(contact, data, phoneNumbers) {
  let matchedContacts = {};
  if (contact) {
    matchedContacts = phoneNumbers.reduce((acc, phoneNumber) => {
      return {
        ...acc,
        [phoneNumber]: [
          {
            id: contact.id,
            type: SERVICE.name,
            name: _.get(contact, `field_values[${PHONE_APPLET_CONFIGURATION.name_field}].value`),
            phoneNumbers: PHONE_FIELD_TYPES
              .map((phoneField) => ({
                phoneNumber: _.get(
                  contact,
                  `field_values[${PHONE_APPLET_CONFIGURATION.phone_field}].value`,
                ),
                phoneType: phoneField.type,
              }))
              .filter((f) => f.phoneNumber),
          },
        ],
      };
    }, {});
  }
  renderContactButtton();
  RingCentral.postMessage(
    {
      type: 'rc-post-message-response',
      responseId: data.requestId,
      response: {
        data: matchedContacts,
      },
    },
    '*',
  );
}

function getCallValuesFromData(data) {
  const callId = _.get(data, 'body.call.id', _.get(data, 'call.callId'));
  let from = _.get(data, 'body.call.from.phoneNumber', _.get(data, 'call.fromNumber') || _.get(data, 'call.from'));
  if (from && from.includes('*')) {
    from = from.split('*')[0];
  }
  let to = _.get(data, 'body.call.to.phoneNumber', _.get(data, 'call.to'));
  if (to && to.includes('*')) {
    to = to.split('*')[0];
  }
  const direction = _.get(data, 'body.call.direction', _.get(data, 'call.direction'));
  const note = _.get(data, 'body.note', '');
  const contact = _.get(currentMatchedContact, 'id');
  const duration = _.get(data, 'body.call.duration', Math.round((_.get(data, 'call.endTime', 0) - _.get(data, 'call.creationTime', 0)) / 1000));


  console.log(contact, currentMatchedContact, 'currentMatchedContact')
  // direction = DIRECTION[(direction||'').toLowerCase()];
  let contactField = leadApp && leadApp === 'lead' ? PHONE_APPLET_CONFIGURATION.fields.lead : PHONE_APPLET_CONFIGURATION.fields.contact;
  return {
    entity: PHONE_APPLET_CONFIGURATION.name,
    field_values: {
      [PHONE_APPLET_CONFIGURATION.fields.type]: 'Phone Call',
      [PHONE_APPLET_CONFIGURATION.fields.from]: from,
      [PHONE_APPLET_CONFIGURATION.fields.to]: to,
      [contactField]: contact,
      [PHONE_APPLET_CONFIGURATION.fields.duration]: duration,
      [PHONE_APPLET_CONFIGURATION.fields.direction]: direction,
      [PHONE_APPLET_CONFIGURATION.fields.ringcentralId]: callId,
      [PHONE_APPLET_CONFIGURATION.fields.note]: note,
    },
  };
}

function handleCallEnd(data) {
  const callId = _.get(data, 'call.callId');

  const callValues = getCallValuesFromData(data);
  console.log({callValues})
  logCallById(callId, callValues, ({ entity_value: phoneCall }) => {
    const entityInstance = {
      ...phoneCall,
      field_values: {
        ...phoneCall.field_values,
        [PHONE_APPLET_CONFIGURATION.fields.status]: {
          ...(phoneCall.field_values[PHONE_APPLET_CONFIGURATION.fields.status]),
          value: true,
        }
      }
    };
    console.log({entityInstance})
    FAClient.showModal('entityFormModal', {
      entity: PHONE_APPLET_CONFIGURATION.name,
      entityLabel: 'Phone Call Outcome Log',
      entityInstance,
      showButtons: false,
    });
  });
}

function getSMSValues(message, contact, app) {
  const ringcentralID = _.get(message, 'id');
  let from = _.get(message, 'from.phoneNumber');
  if (from && from.includes('*')) {
    from = from.split('*')[0];
  }
  let to = _.get(message, 'to[0].phoneNumber');
  if (to && to.includes('*')) {
    to = to.split('*')[0];
  }
  const subject = _.get(message, 'subject');
  const type = _.get(message, 'type');
  const direction = _.get(message, 'direction');
  const messageStatus = _.get(message, 'messageStatus');
  const readStatus = _.get(message, 'readStatus');
  let contactField = app && app === 'lead' ? PHONE_APPLET_CONFIGURATION.fields.lead : PHONE_APPLET_CONFIGURATION.fields.contact;
  console.log({contactFieldSMS: contactField});
  return {
    entity: SMS_APPLET_CONFIGURATION.name,
    field_values: {
      [PHONE_APPLET_CONFIGURATION.fields.type]: 'SMS',
      [PHONE_APPLET_CONFIGURATION.fields.from]: from,
      [PHONE_APPLET_CONFIGURATION.fields.to]: to,
      [contactField]: contact && contact.id,
      [PHONE_APPLET_CONFIGURATION.fields.smsSubject]: subject,
      [PHONE_APPLET_CONFIGURATION.fields.smsMessageType]: type,
      [PHONE_APPLET_CONFIGURATION.fields.smsMessageStatus]: messageStatus,
      [PHONE_APPLET_CONFIGURATION.fields.smsReadStatus]: readStatus,
      [PHONE_APPLET_CONFIGURATION.fields.direction]: direction,
      [PHONE_APPLET_CONFIGURATION.fields.ringcentralId]: `${ringcentralID}`,
    },
  };
}

function createMessage(newMessages, contacts,app, callback) {
  const contact = _.get(contacts, '[0]');
  setCurrentMatchedContact(contact);
  renderContactButtton();
  newMessages.map((message, index) => {
    const isLast = index === newMessages.length - 1;
    const smsValues = getSMSValues(message, contact, app);
    console.log(smsValues);
    FAClient.createEntity(smsValues, (created) => {
      if (isLast) {
        callback();
      }
    });
  });
}
function logMessages(messages, phoneNumbers, callback) {
  const messagesIDs = messages.map((m) => `${m.id}`);
  console.log(phoneNumbers);
  let phoneNumber = phoneNumbers.filter(n => n.length > 4)[0];

  FAClient.listEntityValues(
    {
      entity: PHONE_APPLET_CONFIGURATION.name,
      filters: [
        {
          field_name: PHONE_APPLET_CONFIGURATION.fields.ringcentralId,
          operator: 'includes',
          values: messagesIDs,
        },
      ],
    },
    (existingMessages) => {
      const newMessages = _.differenceWith(
        messages,
        existingMessages,
        (a, b) =>
          `${a.id}` ===
          _.get(
            b,
            `field_values.${SMS_APPLET_CONFIGURATION.fields.ringcentralId}.value`,
          ),
      );

      const pattern = parseNumbersForPattern(phoneNumbers);

      FAClient.listEntityValues(
          {
            entity: PHONE_APPLET_CONFIGURATION.contact_app,
            filters: [
              {
                field_name: PHONE_APPLET_CONFIGURATION.phone_field,
                operator: 'includes',
                values: [phoneNumber],
              },
            ],
            limit: 1,
          },
          (contacts) => {
            console.log({leadsFoundMessage: contacts})
            if(contacts && contacts.length > 0) {
              createMessage(newMessages, contacts, 'lead', callback);
            } else {
              FAClient.listEntityValues(
                  {
                    entity: 'contact',
                    filters: [
                      {
                        field_name: 'work_phone',
                        operator: 'includes',
                        values: [phoneNumber],
                      },
                    ],
                    limit: 1,
                  },(contacts) => {
                    console.log({contactsFound: contacts})
                    if(contacts && contacts.length > 0) {
                      createMessage(newMessages, contacts, 'contact', callback)
                    }
                  });
            }
            //setMatchedContacts(currentMatchedContact, data, phoneNumbers);
          },
      );
    }
  );
}

function handleUpdatedMessage(data) {
  const phoneNumber = _.get(data, 'message.from.phoneNumber');
  const message = _.get(data, 'message');
  const smsValues = getSMSValues(message);

  logMessages([message], [phoneNumber], smsValues, () => {});
}

function cleanFooter() {
  const footer = document.getElementById('footer');
  footer.innerHTML = '';
}

function renderContactButtton() {
  cleanFooter();
  if(!currentMatchedContact) return null;
  const footerButton = document.createElement('button');
  let appView = leadApp || 'contact';
  let phoneField = appView === 'contact' ? 'full_name' : PHONE_APPLET_CONFIGURATION.name_field;
  footerButton.innerText = `Navigate to ${_.get(
    currentMatchedContact,
    `field_values.${phoneField}.value`,
  )}`;
  console.log({currentMatchedContact})

  footerButton.onclick = () => {
    FAClient.navigateTo(`/${appView}/view/${currentMatchedContact.id}`);
  };
  footer.appendChild(footerButton);
}


function ringCentralListener(event) {
  const data = event.data;
  if (!data) return;
  switch (data.type) {
    case 'rc-call-ring-notify':
      console.log({data})
      inboundContact(data);
      ongoingCall = true;
      FAClient.open();
      break;
    case 'rc-call-init-notify':
      console.log({initNotify: data})
      inboundContact(data);
      //setCurrentMatchedContact(null);
      FAClient.open();
      break;
    case 'rc-call-end-notify':
      matchingContacts = false;
      currentCallNumber = null;
      ongoingCall = false;
      console.log({logCallManually: data})
      handleCallEnd(data);
      currentMatchedContact = null;
      cleanFooter();
      break;
    case 'rc-message-updated-notify':
      handleUpdatedMessage(data);
      break;
    case 'rc-inbound-message-notify':
      const phoneNumber = _.get(data, 'message.from.phoneNumber');
      RingCentral.postMessage({
        type: 'rc-adapter-new-sms',
        phoneNumber,
        conversation: true,
      }, '*');
      FAClient.open();
      break;
    case 'rc-route-changed-notify':
      if (!data.path.includes('/calls/active')) {
        cleanFooter();
      }
      if (data.path === '/history') {
        const footerButton = document.createElement('button');
        footerButton.innerText = 'All Phone Calls';
        footerButton.onclick = () => {
          FAClient.navigateTo(
            `/entity/${PHONE_APPLET_CONFIGURATION.name}/view/all`,
          );
        };
        footer.appendChild(footerButton);
      } else if (data.path === '/messages') {
        const footerButton = document.createElement('button');
        footerButton.innerText = 'All SMS';
        footerButton.onclick = () => {
          FAClient.navigateTo(
            `/entity/${PHONE_APPLET_CONFIGURATION.name}/view/all`,
          );
        };
        footer.appendChild(footerButton);
      }
      break;
    case 'rc-login-status-notify':
      if (data.loggedIn) {
        RingCentral.postMessage(
          {
            type: 'rc-adapter-register-third-party-service',
            service: {
              name: SERVICE.name,
              callLoggerPath: '/callLogger',
              callLoggerTitle: `Log to ${SERVICE.name}`,
              messageLoggerPath: '/messageLogger',
              messageLoggerTitle: `Log to ${SERVICE.name}`,
              contactMatchPath: '/contacts/match',
              contactsPath: '/contacts',
              showLogModal: true,
            },
          },
          '*',
        );
      }
      break;
    case 'rc-post-message-request':
      if (data.path === '/callLogger') {
        logCallsManually(data);
      }
      if (data.path === '/messageLogger') {
        logMessagesManually(data);
      }
      if (data.path === '/contacts/match') {
        //matchContacts(data);
      }
      break;
    case 'rc-callLogger-auto-log-notify':
      break;
    default:
      break;
  }
}

function removeTextMessage() {
  const loadingText = document.getElementById('loading-text');
  loadingText.remove();
}
